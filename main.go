// +build !appengine

// Copyright (C) 2013 Andras Belicza. All rights reserved.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

// Contains the main function of the Gowut "Showcase of Features" demo.
// Separated because main() can't be defined on AppEngine.

package main

import (
	//"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	//	"os"
	"log"
	"os"
	"path/filepath"
	//	"reflect"
	"github.com/ghodss/yaml"
	//"gopkg.in/yaml.v2"
	//"strconv"
	//	"os/exec"
	"time"
)

//-cfg gups.conf -g -c -aa 192.168.0.20

var s_localIp string

type Conf struct {
	IsAgentMode             bool      `json:"isAgentMode"`
	IsGuiEnabled            bool      `json:"isGuiEnabled"`
	SerialPort              string    `json:"serialPort"`
	AgentAddr               string    `json:"agentAddr"`
	SnmpPort                string    `json:"snmpPort"`
	TrapPort                string    `json:"trapPort"`
	TargetIp                [5]string `json:"targetIp"`
	ShutdownWaitTime        string    `json:"shutdownWaitTime"`
	UpsIdentName            string    `json:"upsIdentName"`
	UpsIdentAttachedDevices string    `json:"upsIdentAttachedDevices"`
	BatteryVoltage          int       `json:"batteryVoltage"`
	Community               string    `json:"community"`
	Http_port_agent         string    `json:"http_port_agent"`
	Http_port_client        string    `json:"http_port_client"`
	Demonize                bool      `json:"demonize"`
}
type ResConf struct {
	serialPort, snmpAgentAddr, snmpAgentPort, snmpTrapPort,
	shutdownWaitTime string
	targetAddr                [5]string
	isAgentMode, isGuiEnabled bool
	upsIdentName              string
	upsIdentAttachedDevices   string
	batteryVoltage            int
	community                 string
	http_port_agent           string
	http_port_client          string
	demonize                  bool
}

var rcnf ResConf
var startTime time.Time

type type_cm struct {
	configFileP,
	serialPortP,
	snmpAgentAddrP,
	snmpAgentPortP,
	snmpTrapPortP,
	targetIpP0,
	targetIpP1,
	targetIpP2,
	targetIpP3,
	targetIpP4,
	str_shutdownWaitTimeP,
	str_communityP,
	str_http_port_agentP,
	str_http_port_clientP *string

	demonizeP,
	clientP,
	agentxP,
	guiP *bool
}

func start_routines() {
	if rcnf.isAgentMode == false {
		str_localhost_port = rcnf.http_port_client
		go client()
		fmt.Printf("* режим клиента SNMP *\n")
	} else {
		str_localhost_port = rcnf.http_port_agent
		init_agent_x()
		fmt.Printf("* режим субагента SNMP *\n")
		s_localIp = getLocalIP()
		//fmt.Printf("local IP = %s\n", s_localIp)
		go serial_routine(rcnf.serialPort)
	}

	if rcnf.isGuiEnabled == true {
		go display_data()
	}
}

var cm type_cm
var conf Conf

func main() {
	startTime = time.Now()
	log.SetFlags(log.Lshortfile)

	cm.configFileP = flag.String("cfg", "", "config file")
	cm.serialPortP = flag.String("s", "", "path to serial device (etc. /dev/ttyS0")
	cm.snmpAgentAddrP = flag.String("aa", "", "SNMP agent address")
	cm.snmpAgentPortP = flag.String("ap", "", "SNMP agent port")
	cm.snmpTrapPortP = flag.String("at", "", "SNMP agent trap/notification port")
	cm.targetIpP0 = flag.String("ta1", "", "target addr#1")
	cm.targetIpP1 = flag.String("ta2", "", "target addr#2")
	cm.targetIpP2 = flag.String("ta3", "", "target addr#3")
	cm.targetIpP3 = flag.String("ta4", "", "target addr#4")
	cm.targetIpP4 = flag.String("ta5", "", "target addr#5")
	//	helpP := flag.String("h", "42", "Help text and info data")
	cm.str_shutdownWaitTimeP = flag.String("st", "", "shutdown time in seconds")
	cm.str_communityP = flag.String("community", "public", "community string: public (default) or private or etc.)")
	cm.str_http_port_agentP = flag.String("pa", "localhost:8081", "http порт агента (8081 умолчанию))")
	cm.str_http_port_clientP = flag.String("pc", "localhost:8082", "http порт агента (8082 умолчанию))")
	cm.demonizeP = flag.Bool("f", false, "do NOT demonize app")
	cm.clientP = flag.Bool("c", false, "as snmp client")
	cm.agentxP = flag.Bool("a", false, "with agentx")
	cm.guiP = flag.Bool("g", false, "with gui")

	flag.Parse()

	var data []byte
	var err error
	dir, errDir := filepath.Abs(filepath.Dir(os.Args[0]))
	if errDir != nil {
		log.Fatal(err)
	}
	if *cm.configFileP == "" {
		//data, err := ioutil.ReadFile("/home/dambo/go/src/adm/gups/gups.conf")
		data, err = ioutil.ReadFile(dir + "/gups.conf")
		//fmt.Printf(".conf = ")
	} else {
		data, err = ioutil.ReadFile(dir + "/" + *cm.configFileP)
	}
	fmt.Printf("%s\n", *cm.configFileP)
	if err != nil {
		log.Fatalln(err)
		return
	}

	//var conf interface{}

	err = yaml.Unmarshal(data, &conf)
	if err != nil {
		log.Fatalln(err)
	}
	fmt.Printf("input config: %#v\n", conf)

	// если флаг запуска в режиме демона
	fmt.Printf("demonize flag: %p, %t\n", cm.demonizeP, *cm.demonizeP)
	if *cm.demonizeP == true {
		rcnf.demonize = true
	} else {
		rcnf.demonize = conf.Demonize
	}
	fmt.Printf("demonize flag: %t\n", rcnf.demonize)
	if rcnf.demonize == true {
		daemon_main()
	} else {
		some_init()
		start_routines()
		doneAll := make(chan bool)
		<-doneAll

	}
}

func some_init() {
	//	 если флаг режима клиента (-с)
	if *cm.clientP == true {
		rcnf.isAgentMode = false
		// если нет флага, но есть в конфиг-файле
	} else if conf.IsAgentMode == false {
		rcnf.isAgentMode = false //!conf.(*Conf).isAgentMode
		// если нет ни флага, ни в конфиг-файле, то по умолч.режим клиента
	} else {
		rcnf.isAgentMode = false
	}
	// если флаг режима агента (-a)
	if *cm.agentxP == true {
		rcnf.isAgentMode = true
		// если нет флага, но есть в конфиг-файле
	} else if conf.IsAgentMode == true && *cm.clientP == false {
		rcnf.isAgentMode = conf.IsAgentMode
		// если нет ни флага, ни в конфиг-файле, то по умолч.режим клиента
	} else {
		rcnf.isAgentMode = false
	}
	if *cm.snmpAgentAddrP != "" {
		rcnf.snmpAgentAddr = *cm.snmpAgentAddrP
	} else {
		rcnf.snmpAgentAddr = conf.AgentAddr
	}
	if *cm.snmpAgentPortP != "" {
		rcnf.snmpAgentPort = *cm.snmpAgentPortP
	} else {
		rcnf.snmpAgentPort = conf.SnmpPort
	}
	if *cm.snmpTrapPortP != "" {
		rcnf.snmpTrapPort = *cm.snmpTrapPortP
	} else {
		rcnf.snmpTrapPort = conf.TrapPort
	}
	if rcnf.isAgentMode == true {
		if *cm.serialPortP != "" {
			rcnf.serialPort = *cm.serialPortP
		} else {
			rcnf.serialPort = conf.SerialPort
		}
	}
	// если флаг запуска GUI (браузера) (-g)
	if *cm.guiP == true {
		rcnf.isGuiEnabled = *cm.guiP
	} else if conf.IsGuiEnabled == true {
		rcnf.isGuiEnabled = conf.IsGuiEnabled
		// если нет ни флага, ни в конфиг-файле, то по умолч.режим агента
	} /*else {
		rcnf.isAgentMode = true
	}*/

	if *cm.targetIpP0 != "" {
		rcnf.targetAddr[0] = *cm.targetIpP0
	} else {
		rcnf.targetAddr[0] = conf.TargetIp[0]
	}
	if *cm.targetIpP1 != "" {
		rcnf.targetAddr[1] = *cm.targetIpP1
	} else {
		rcnf.targetAddr[1] = conf.TargetIp[1]
	}
	if *cm.targetIpP2 != "" {
		rcnf.targetAddr[2] = *cm.targetIpP2
	} else {
		rcnf.targetAddr[2] = conf.TargetIp[2]
	}
	if *cm.targetIpP3 != "" {
		rcnf.targetAddr[3] = *cm.targetIpP3
	} else {
		rcnf.targetAddr[3] = conf.TargetIp[3]
	}
	if *cm.targetIpP4 != "" {
		rcnf.targetAddr[4] = *cm.targetIpP4
	} else {
		rcnf.targetAddr[4] = conf.TargetIp[4]
	}

	if *cm.str_shutdownWaitTimeP != "" {
		rcnf.shutdownWaitTime = *cm.str_shutdownWaitTimeP
	} else if conf.ShutdownWaitTime != "" {
		rcnf.shutdownWaitTime = conf.ShutdownWaitTime
	} else {
		rcnf.shutdownWaitTime = ""
	}

	if *cm.str_communityP != "" {
		rcnf.community = *cm.str_communityP
	} else if conf.Community != "" {
		rcnf.community = conf.Community
	} else {
		rcnf.community = ""
	}

	if *cm.str_http_port_agentP != "" {
		rcnf.http_port_agent = *cm.str_http_port_agentP
	} else if conf.Http_port_agent != "" {
		rcnf.http_port_agent = conf.Http_port_agent
	} else {
		rcnf.http_port_agent = "localhost:8081"
	}
	if *cm.str_http_port_clientP != "" {
		rcnf.http_port_client = *cm.str_http_port_clientP
	} else if conf.Http_port_agent != "" {
		rcnf.http_port_client = conf.Http_port_client
	} else {
		rcnf.http_port_client = "localhost:8082"
	}

	rcnf.upsIdentName = conf.UpsIdentName
	rcnf.upsIdentAttachedDevices = conf.UpsIdentAttachedDevices
	rcnf.batteryVoltage = conf.BatteryVoltage

	fmt.Printf("Configuration:\n %#v\n", rcnf)

}
