// отображение данных в браузере НА СТОРОНЕ _Клиента_
package main

import (
	"fmt"
	"log"
	"os"
	"os/exec"
	//	"strconv"
	"math"
	//	"strconv"
	//	"reflect"
	"time"
	//	vl "github.com/posteo/go-agentx/value"

	g "github.com/soniah/gosnmp"
)

var notify *Notificator
var alarmOidStr [7]string

func note(s string) {

	notify = New(Notificator_Options{
		DefaultIcon: "icon/default.png",
		AppName:     "UPS Titan",
	})

	notify.Push("ИБП Титан", s, "/home/user/icon.png", UR_CRITICAL)

}

func ToString(value interface{}) string {
	var val string
	switch value := value.(type) { // shadow
	case string:
		val = string(value)
		//fmt.Printf("ToString(..) val=%s", val)
	}
	return val
}

func ToBool(value interface{}) bool {
	var val bool
	switch value := value.(type) { // shadow
	case bool:
		val = bool(value)
	}
	return val
}
func ToInt(value interface{}) int {
	var val int
	switch value := value.(type) { // shadow
	case int:
		val = int(value)
	}
	return val
}
func ToUint32(value interface{}) uint32 {
	var val uint32
	switch value := value.(type) { // shadow
	case int:
		val = uint32(value)
	}
	return val
}

//func fromOidToString(value interface{}) string {
//	var strOid string
//	switch value := value.(type) { // shadow
//	case string:
//		fmt.Println("!!!\n")
//		strOid = string(value)
//	}
//	return str.Oid
//}

//var blntoInt = []bool{false, true}

//func boolToInt(ft int) bool {
//	return blntoInt[ft]
//}

var isShutdownOn bool = false

func removeFromSlice(s []string, r string) []string {
	for i, v := range s {
		if v == r {
			return append(s[:i], s[i+1:]...)
		}
	}
	return s
}

func isArContained(s [7]string, r string) bool {
	//fmt.Println("/n=============\n")
	for _, v := range s {
		//fmt.Printf("%d=%s", i, s)
		if v == r {
			return true
		}
	}
	return false
}

func isContained(s []string, r string) bool {
	//fmt.Println("/n=============\n")
	for _, v := range s {
		//fmt.Printf("%d=%s", i, s)
		if v == r {
			return true
		}
	}
	return false
}

func client() {

	// Default is a pointer to a GoSNMP struct that contains sensible defaults
	// eg port 161, community public, etc
	//	g.Default.Target = "192.168.10.48"
	// /g.Default.Target = "127.0.0.1"
	//err := g.Default.Connect()

	params := &g.GoSNMP{
		//Target: envTarget,
		//Target: "192.168.0.20",
		//Target: "127.0.0.1",
		Target: rcnf.snmpAgentAddr,
		//Port:      uint16(port),
		Port:      uint16(161),
		Community: rcnf.community, //"public"
		Version:   g.Version2c,
		Timeout:   time.Duration(2) * time.Second,
		//Logger:    log.New(os.Stdout, "", 0),
		Logger: nil,
	}
	err := params.Connect()
	if err != nil {
		log.Fatalf("Connect() err !!: %v", err)
	}
	defer params.Conn.Close()

	str_oids_int := []string{
		s_upsInputVoltage,
		s_upsOutputVoltage,
		s_upsOutputPercentLoad,
		s_upsInputFrequency,
		s_upsBatteryVoltage,
		s_upsBatteryTemperature,
	}
	str_oids_alrm := []string{
		s_upsAlarmDescr1,
		s_upsAlarmDescr2,
		s_upsAlarmDescr3,
		s_upsAlarmDescr4,
		s_upsAlarmDescr5,
		s_upsAlarmDescr6,
	}
	var dataFormat = [lb_val_N]thepair{
		{1, 6, "%3.1f", 3, 1}, //MMM.M
		//		{7, 12, "%3.1f", 3, 1},  //NNN.N
		{13, 18, "%3.1f", 3, 1}, //PPP.P
		{19, 22, "%3.0f", 3, 0}, //QQQ
		{23, 27, "%2.1f", 2, 1}, //RR.R
		{28, 32, "%1.1f", 1, 1}, //S.SS
		{33, 37, "%2.1f", 2, 1}, //TT.T
	}
	alarmCnt = 0 // Вначале число алармов=0
	var oldAr [7]string
	for {
		for i, p := range str_oids_int {
			var oids = []string{p}
			result, err2 := params.Get(oids) // Get() accepts up to g.MAX_OIDS
			if err2 != nil {
				log.Fatalf("Get() err: !!!%v", err2)
			}
			for _, variable := range result.Variables {
				switch variable.Type {
				case g.OctetString:
					val_str[i] = ToString(variable.Value)
					//fmt.Printf("CLIENT1: %s\n", val_str[i])
				case g.Integer:
					//fmt.Printf("g.Integer: %d[%d]\n", g.ToBigInt(variable.Value), i)
					// Значение запрошенного oid'а:
					value := g.ToBigInt(variable.Value).Int64()
					vf[i] = float64(value) / math.Pow10(dataFormat[i].e)
					val_str[i] = fmt.Sprintf(dataFormat[i].c, vf[i])
					//fmt.Printf("g.Integer*: %s[%d]\n", val_str[i], i)
				case g.Counter32:
					// Значение запрошенного oid'а:
					value := g.ToBigInt(variable.Value).Uint64()
					//fmt.Printf("CLIENT0: %d[%d]\n", value, i)
					vf[i] = float64(value) / math.Pow10(dataFormat[i].e)
					val_str[i] = fmt.Sprintf(dataFormat[i].c, vf[i])
					//fmt.Printf("CLIENT1: %s[%d]\n", val_str[i], i)
				case g.Gauge32:
					// Значение запрошенного oid'а:
					value := g.ToBigInt(variable.Value).Uint64()
					//fmt.Printf("CLIENT0: %d[%d]\n", value, i)
					vf[i] = float64(value) / math.Pow10(dataFormat[i].e)
					val_str[i] = fmt.Sprintf(dataFormat[i].c, vf[i])
					//fmt.Printf("CLIENT1: %s[%d]\n", val_str[i], i)
				case g.Uinteger32:
					// Значение запрошенного oid'а:
					value := g.ToBigInt(variable.Value).Uint64()
					//fmt.Printf("CLIENT0: %d[%d]\n", value, i)
					vf[i] = float64(value) / math.Pow10(dataFormat[i].e)
					val_str[i] = fmt.Sprintf(dataFormat[i].c, vf[i])
					//fmt.Printf("CLIENT1: %s[%d]\n", val_str[i], i)
				default:
				}
				//time.Sleep(time.Millisecond * 50)
			}
		}
		/// *** ***
		// для статистики проверяем текущ.кол-во алармов:
		var oids = []string{s_upsAlarmsPresent}
		result, err_nAlarm := params.Get(oids) // Get() accepts up to g.MAX_OIDS
		if err_nAlarm != nil {
			fmt.Printf("Get() err: !!!%v", err_nAlarm)
			continue
		}
		alarmCnt := g.ToBigInt(result.Variables[0].Value).Uint64()
		//fmt.Printf("alarmCnt =  %d\n", alarmCnt)

		//var arPos int = 0
		var ar []string
		var strs []string
		// запрашиваем и проверяем все алармы (6 шт)
		// TODO ИЛИ? запрашиваем и проверяем только alarmCnt алармов (0..6 шт)
		for i, p := range str_oids_alrm {
			var oids = []string{p}
			res, err2 := params.Get(oids) // Get() accepts up to g.MAX_OIDS
			if err2 != nil {
				fmt.Printf("Get() err: !!!%v", err2)
				continue
			}
			strOid := ToString(res.Variables[0].Value)
			//fmt.Printf("alarm[%d] = %s ", i, strOid)

			if strOid != "" {
				str1 := strOid[1:]
				if isArContained(oldAr, str1) == false { // если ранее не было этого аларма (чтобы не плодить всплывающие окошки)
					//					for i, p := range oldAr {
					//						fmt.Printf("oldSlice[%d]=%s\n", i, p)
					//					}
					//fmt.Printf("str1 = %s\n", str1)
					if str1 == s_upsAlarmOnBattery {
						if rcnf.shutdownWaitTime != "none" &&
							rcnf.shutdownWaitTime != "" {
							//cmd := exec.Command("shutdown", rcnf.shutdownWaitTime)
							cmd := exec.Command("shutdown /t ", rcnf.shutdownWaitTime)
							cmd.Stdout = os.Stdout
							cmd.Stderr = os.Stderr
							if err := cmd.Run(); err != nil {
								fmt.Println(err)
							} else {
								//note("Компьютер будет выключен через " + rcnf.shutdownWaitTime + " минут!")
								note("Компьютер будет выключен через " + rcnf.shutdownWaitTime + " секунд!")
							}
						}
					} else if str1 == s_upsAlarmLowBattery {

						//cmd := exec.Command("shutdown", "-P") // для linux
						cmd := exec.Command("shutdown", "/s") // для windowz (TODO: вставить в gups.conf!)

						cmd.Stdout = os.Stdout
						cmd.Stderr = os.Stderr
						if err := cmd.Run(); err != nil {
							fmt.Println(err)
						} else {
							note("Компьютер будет выключен немедленно!")
						}
						note("Компьютер будет выключен немедленно!")
					}
					note(oidStrToAlarmTxt[strOid[1:]])
				}
				strs = append(strs, oidStrToAlarmTxt[str1]+string("  ✓"))
				ar = append(ar, str1) // нужно
			}
			_ = i
			_ = alarmCnt
			//			if uint64(i+1) == alarmCnt {
			//				break
			//			}
		}
		// добавляем все акт.алармы в listbox
		listBox.SetValues(strs)
		// если ранее был аларм, но пропал
		if rcnf.shutdownWaitTime != "none" && rcnf.shutdownWaitTime != "" {
			if (isArContained(oldAr, s_upsAlarmOnBattery) == true) && (isContained(ar, s_upsAlarmOnBattery) == false) {
				fmt.Printf("shutdown -s")
				cmd := exec.Command("shutdown", "-c")
				cmd.Stdout = os.Stdout
				cmd.Stderr = os.Stderr
				if err := cmd.Run(); err != nil {
					fmt.Println(err)
				} else {
					note("Питание в норме, отключение отменяется")
				}
			}
		}

		// копируем ar в очищенный oldAr
		for i, _ := range oldAr {
			oldAr[i] = ""
		}
		for i, p := range ar {
			oldAr[i] = p
		}

		//		// Всплывающие сообщения на вх.алармы
		//		for _, p := range strs {
		//			note(p)
		//		}
		time.Sleep(time.Millisecond * 500)
	}

}

// Get sends an SNMP GET request
//func (x *GoSNMP) Get_oid(oid string) (result *SnmpPacket, err error) {
//	// convert oid to pdu slice
//	var pdus []SnmpPDU
//	pdus = append(pdus, SnmpPDU{oid, Null, nil, x.Logger})
//	// build up SnmpPacket
//	packetOut := x.mkSnmpPacket(GetRequest, pdus, 0, 0)
//	return x.send(packetOut, true)
//}
