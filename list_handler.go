/*
go-agentx
Copyright (C) 2015 Philipp Brüll <bruell@simia.tech>

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
USA
*/

package main

import (
	"bytes"
	//	"fmt"
	"sort"
	"time"

	"github.com/posteo/go-agentx/pdu"
	"github.com/posteo/go-agentx/value"
)

const Const_data_MAX = 7

// аналоговые значения из ups в формате с фикс.точкой (фикс.множитель)
var Vi [Const_data_MAX]int32

// ListHandler is a helper that takes a list of oids and implements
// a default behaviour for that list.
type ListHandler struct {
	oids  sort.StringSlice
	items map[string]*ListItem
}

// Add adds a list item for the provided oid and returns it.
func (l *ListHandler) Add(oid string) *ListItem {
	if l.items == nil {
		l.items = make(map[string]*ListItem)
	}

	l.oids = append(l.oids, oid)
	l.oids.Sort()
	item := &ListItem{}
	l.items[oid] = item
	return item
}

// Delete item by oid
func (l *ListHandler) Delete(oid string) {
	delete(l.items, oid)

	for i, p := range l.oids {
		if p == oid {
			l.oids = append(l.oids[:i], l.oids[i+1:]...)
			//l.oids.Sort()
			return
		}
	}

	return
}

// Get tries to find the provided oid and returns the corresponding value.
func (l *ListHandler) Get(oid value.OID) (value.OID, pdu.VariableType, interface{}, error) {
	if l.items == nil {
		return nil, pdu.VariableTypeNoSuchObject, nil, nil
	}
	str_oid := oid.String()
	item, ok := l.items[str_oid]
	if ok {
		switch str_oid {
		case s_upsInputLineBads:
			if vAlarm[al_UtilityFail] == 1 {
				return oid, item.Type, uint32(1), nil
			} else { // 220В ок
				return oid, item.Type, uint32(0), nil
			}
		case s_upsInputNumLines:
			return oid, item.Type, int32(1), nil
		case s_upsInputLineIndex:
			return oid, item.Type, int32(1), nil
		case s_upsInputCurrent:
			return oid, item.Type, int32(0), nil
		case s_upsInputTruePower:
			return oid, item.Type, int32(0), nil
		case s_upsOutputSource:
			if vAlarm[al_BypassActive] == 1 { // байпас вкл
				return oid, item.Type, int32(4), nil
			} else if vAlarm[al_UtilityFail] == 1 { // от батареи
				return oid, item.Type, int32(5), nil
			} else {
				return oid, item.Type, int32(1), nil
			}

			return oid, item.Type, int32(4), nil

		case s_upsInputVoltage:
			return oid, item.Type, int32(Vi[0]), nil
		case s_upsOutputVoltage:
			return oid, item.Type, int32(Vi[2]), nil
		case s_upsOutputPercentLoad:
			return oid, item.Type, int32(Vi[3]), nil
		case s_upsInputFrequency:
			return oid, item.Type, int32(Vi[4]), nil
		case s_upsBatteryVoltage:
			//return oid, item.Type, int32(int32(rcnf.batteryVoltage) * Vi[5] / 2 / 10), nil
			return oid, item.Type, int32((float64(rcnf.batteryVoltage)*float64(Vi[5])/2 + 0.5) / 10), nil
		case s_upsBatteryTemperature:
			return oid, item.Type, int32(Vi[6]), nil

		case s_upsAlarmsPresent:
			return oid, item.Type, uint32(alarmCnt), nil

		case s_upsBatteryStatus:
			if vAlarm[al_BatteryLow] == 1 {
				return oid, item.Type, int32(3), nil
			} else { // Батарея ок
				return oid, item.Type, int32(2), nil
			}

		case s_upsSecondsOnBattery:
			if vAlarm[al_UtilityFail] == 1 {
				item.Value = int32(time.Now().Sub(batteryStartTime).Seconds())
			} else {
				item.Value = int32(0)
			}
			return oid, item.Type, item.Value, nil

		case s_upsIdentName:
			return oid, item.Type, string(rcnf.upsIdentName), nil
			//return oid, item.Type, string("123123"), nil

		case s_upsIdentAttachedDevices:
			return oid, item.Type, string(rcnf.upsIdentAttachedDevices), nil

		case
			s_upsIdentManufacturer, s_upsIdentModel,
			s_upsIdentUPSSoftwareVersion, s_upsIdentAgentSoftwareVersion,
			s_upsEstimatedMinutesRemaining,
			s_upsEstimatedChargeRemaining,
			//s_upsBatteryVoltage,
			s_upsBatteryCurrent,
			//s_upsBatteryTemperature,

			//s_upsOutputSource,
			s_upsOutputFrequency,
			s_upsOutputNumLines,
			s_upsOutputLineIndex,
			//s_upsOutputVoltage,
			s_upsOutputCurrent,
			s_upsOutputPower,
			//s_upsOutputPercentLoad,

			s_upsAlarmId1, s_upsAlarmId2, s_upsAlarmId3,
			s_upsAlarmId4, s_upsAlarmId5, s_upsAlarmId6,
			s_upsAlarmDescr1, s_upsAlarmDescr2, s_upsAlarmDescr3,
			s_upsAlarmDescr4, s_upsAlarmDescr5, s_upsAlarmDescr6,
			s_upsAlarmTime1, s_upsAlarmTime2, s_upsAlarmTime3,
			s_upsAlarmTime4, s_upsAlarmTime5, s_upsAlarmTime6:
			return oid, item.Type, item.Value, nil
		}
		//		default:
		//			return oid, item.Type, item.Value, nil
	}
	return nil, pdu.VariableTypeNoSuchObject, nil, nil
}

// GetNext tries to find the value that follows the provided oid and returns it.
func (l *ListHandler) GetNext(from value.OID, includeFrom bool, to value.OID) (value.OID, pdu.VariableType, interface{}, error) {
	if l.items == nil {
		return nil, pdu.VariableTypeNoSuchObject, nil, nil
	}

	fromOID, toOID := from.String(), to.String()
	for _, oid := range l.oids {
		if oidWithin(oid, fromOID, includeFrom, toOID) {
			return l.Get(value.MustParseOID(oid))
		}
	}

	return nil, pdu.VariableTypeNoSuchObject, nil, nil
}

func oidWithin(oid string, from string, includeFrom bool, to string) bool {
	oidBytes, fromBytes, toBytes := []byte(oid), []byte(from), []byte(to)

	fromCompare := bytes.Compare(fromBytes, oidBytes)
	toCompare := bytes.Compare(toBytes, oidBytes)

	return (fromCompare == -1 || (fromCompare == 0 && includeFrom)) && (toCompare == 1)
}
