// отображение данных в браузере НА СТОРОНЕ _АГЕНТА_
package main

import (
	"fmt"
	"log"
	//	"strings"
	//	"math"
	"strconv"
	"time"

	"github.com/icza/gowut/gwu"

	//	"github.com/posteo/go-agentx/pdu"
)

type h_btnAlrmOn struct {
	counter int
	text    string
}
type h_btnAlrmOff struct {
	counter int
	text    string
}
type h_btnBypass struct {
	counter int
	text    string
}

const lb_val_N int = const_SNMP_OID_MAX - 1

//var names = [const_SNMP_OID_MAX]string{"I/P voltage", "I/P fault voltage", "O/P voltage", "O/P load", "I/P frequency", "Battery voltage", "Temperature"}
var names_str_oid = [lb_val_N]string{"Входное напряжение (В)", //"I/P fault voltage",
	"Выходное напряжение (В)", "Выходная нагрузка (%)", "Частота входного напряжения (Гц)", "Напряжение батареи (В)", "Температура (°C)"}

var lb [lb_val_N]gwu.Label
var btnAlrmOn, btnAlrmOff, btnBypass gwu.Button

const alrmLbNum int = 7

var val_str [alrmLbNum]string

var alrmLb [alrmLbNum]gwu.Label
var tb_alarmMask gwu.TextBox

//var lbBypass gwu.Label
//var names_str_alrmLb = [alrmLbNum]string{"Питание от батареи", "Батарея разряжена", "Авария сети", "Питание через байпас", "Общая авария", "Нет связи с ИБП" /*"Идет тест"*/}
var names_str_alrmLb = [alrmLbNum]string{"Авария сети/Питание от батареи", "Батарея разряжена", "Питание через байпас", "Общая авария", "Идет тест", "Нет связи с ИБП"}

//var str_localhosts = []string{"localhost:8081", "localhost:8082"}
var str_localhost_port string

var tabAlrm gwu.Table
var listBox gwu.ListBox

// setNoWrap sets WhiteSpaceNowrap to all children of the specified panel.
//func setNoWrap(panel gwu.Panel) {
//	count := panel.CompsCount()
//	for i := count - 1; i >= 0; i-- {
//		panel.CompAt(i).Style().SetWhiteSpace(gwu.WhiteSpaceNowrap)
//	}
//}

// SessHandler is our session handler to build the showcases window.
type sessHandler struct{}

func (h sessHandler) Created(s gwu.Session) {
	//buildShowcaseWin(s)
}

func (h sessHandler) Removed(s gwu.Session) {}

func StartServer(appName, addr string, autoOpen bool) {
	// Create GUI server
	server := gwu.NewServer(appName, addr)
	//	for _, headHTML := range extraHeadHTMLs {
	//		server.AddRootHeadHTML(headHTML)
	//	}
	//server.AddStaticDir("pict", "http://127.0.0.1:8082/home/dambo/go/src/adm/gups/pict")
	//	server.AddStaticDir("pict", "/home/dambo/go/src/adm/gups/pict")
	//	server.SetText("UPS-терминал")

	server.AddSessCreatorName("main", "параметры UPS Титан")
	server.AddSHandler(sessHandler{})
	// Just for the demo: Add an extra "Gowut-Server" header to all responses holding the Gowut version
	//	server.SetHeaders(map[string][]string{
	//		"Gowut-Server": {gwu.GowutVersion},
	//	})

	// Start GUI server
	var openWins []string
	if autoOpen {
		openWins = []string{"main"}
	}
	if err := server.Start(openWins...); err != nil {
		log.Println("Error: Cound not start GUI server:", err)
		return
	}
}

var str_OnOffBypass = []string{"", " выкл", " вкл"}
var bypassOn bool = false
var onBtn1 bool = false
var b2i = map[bool]int{false: 1, true: 2}
var sigBypass bool

// обработчик нажатия кнопки имитации включения аларма
func (h *h_btnAlrmOn) HandleEvent(e gwu.Event) {
	if b, isButton := e.Src().(gwu.Button); isButton {
		txt := tb_alarmMask.Text()
		nAlr, err := strconv.Atoi(txt)
		if err == nil {
			for i := 0; i < const_alarm_MAX; i++ {
				if (nAlr & 1) == 1 {
					buf[sglist[i]] = byte('1')
				}
				nAlr = nAlr >> 1
				b.SetText(str_alarmOidsNames[i] + " вкл")
				fmt.Printf("Уст байт %d", nAlr)
			}
		} else {
			fmt.Println("Range is out\n")
		}
		e.MarkDirty(b)
	}
}

// обработчик нажатия кнопки имитации выключения аларма
func (h *h_btnAlrmOff) HandleEvent(e gwu.Event) {
	if b, isButton := e.Src().(gwu.Button); isButton {
		txt := tb_alarmMask.Text()
		nAlr, err := strconv.Atoi(txt)
		if err == nil {
			for i := 0; i < const_alarm_MAX; i++ {
				if (nAlr & 1) == 1 {
					buf[sglist[i]] = byte('0')
				}
				nAlr = nAlr >> 1
				b.SetText(str_alarmOidsNames[i] + " выкл")
				fmt.Printf("Cброшен байт %d", nAlr)
			}
		} else {
			fmt.Println("Range is out\n")
		}
		e.MarkDirty(b)
	}
}

// обработчик нажатия кнопки вкл байпаса
func (h *h_btnBypass) HandleEvent(e gwu.Event) {
	if b, isButton := e.Src().(gwu.Button); isButton {
		if bypassState == false {
			_, err := serialport.Write([]byte("BO1\n\r"))
			if err != nil {
				log.Fatal(err)
			}
			b.SetText("Байпас включается...")
		} else {
			_, err := serialport.Write([]byte("BO0\n\r"))
			if err != nil {
				log.Fatal(err)
			}
			b.SetText("Байпас выключается...")
		}
		e.MarkDirty(b)
	}
}

func display_data() {

	server := gwu.NewServer("UPS", str_localhost_port)
	server.SetText("* ИБП Титан *")
	server.AddStaticDir("pic", "/home/dambo/go/src/adm/gups/pict")

	// Create and build a window
	win := gwu.NewWindow("main", "UPS info")
	win.Style().SetFullWidth()
	win.SetHAlign(gwu.HACenter)
	win.SetCellPadding(2)

	//img := gwu.NewImage("PSDlogo", "http://logo.png")
	//img := gwu.NewImage("PSDlogo", "http://www.promsd.ru/images/logo_PS.gif")
	//	img.Style().SetSizePx(275, 95)
	//	win.Add(img)

	win.Add(gwu.NewLabel("Состояние и управление UPS"))
	//lbBypass = gwu.NewLabel("Байпас выключен")
	//win.Add(lbBypass)
	if rcnf.isAgentMode == true {
		btnAlrmOn := gwu.NewButton("Имитация включения байта")
		btnAlrmOn.AddEHandler(&h_btnAlrmOn{text: ":-)"}, gwu.ETypeClick, gwu.ETypeChange, gwu.ETypeStateChange)
		btnAlrmOff := gwu.NewButton("Имитация выключения байта")
		btnAlrmOff.AddEHandler(&h_btnAlrmOff{text: ":-)"}, gwu.ETypeClick, gwu.ETypeChange, gwu.ETypeStateChange)
		btnBypass = gwu.NewButton("Байпас выключен")
		btnBypass.AddEHandler(&h_btnBypass{text: ":-)"}, gwu.ETypeClick, gwu.ETypeChange, gwu.ETypeStateChange)
		// textBox для номера аларма (1..6)
		tb_alarmMask = gwu.NewTextBox("1")
		tb_alarmMask.SetMaxLength(2)
		// панель для кнопки и бокса с номером
		pan2 := gwu.NewHorizontalPanel()
		pan2.Style().SetBorder2(1, gwu.BrdStyleSolid, gwu.ClrBlack)
		pan2.SetCellPadding(1)
		//		pan2.Add(btnAlrmOn)
		//		pan2.Add(btnAlrmOff)
		pan2.Add(btnBypass)
		//pan2.Add(tb_alarmMask)
		win.Add(pan2)
	}
	timelabel := gwu.NewLabel("Текущее время")
	timerTime := gwu.NewTimer(time.Millisecond * 500)
	timerTime.SetRepeat(true)
	//for i := 0; i < const_SNMP_OID_MAX; i++ {
	for i, _ := range names_str_oid {
		win.AddVSpace(10)
		lb[i] = gwu.NewLabel("---")
		win.Add(lb[i])
	}
	win.AddVSpace(20)

	if rcnf.isAgentMode == true {
		// создаем таблицу, чтобы вставить в нее лэйблы для алармов
		tabAlrm = gwu.NewTable()
		tabAlrm.Style().SetBorder2(1, gwu.BrdStyleSolid, gwu.ClrGrey)
		tabAlrm.SetAlign(gwu.HARight, gwu.VATop)
		tabAlrm.EnsureSize(5, 5)

		// Добавляем в таблицу алармы
		for row, p := range names_str_alrmLb {
			alrmLb[row] = gwu.NewLabel(p)
			alrmLb[row].Style().SetColor(gwu.ClrTeal)
			tabAlrm.Add(alrmLb[row], row, 0)
			if row%2 == 0 {
				//			tabAlrm.RowFmt(row).Style().SetBackground(gwu.ClrSilver)
				tabAlrm.RowFmt(row).Style().SetBackground("#C4C4C4")
			} else {
				tabAlrm.RowFmt(row).Style().SetBackground("#C0C0C0")
				//tabAlrm.RowFmt(row).Style().SetBackground(gwu.ClrGrey)
			}
			tabAlrm.CellFmt(row, 0).Style().SetSizePx(300, 40)
			tabAlrm.CellFmt(row, 0).SetAlign(gwu.HALeft, gwu.VAMiddle)
			//tabAlrm.CompAt(row, 0).Style().SetFullSize()
			tabAlrm.CompAt(row, 0).Style().SetFullWidth()
			//tabAlrm.SetColSpan(row, 0, 2)
			tabAlrm.SetRowSpan(row, 0, 1)
		}
		win.Add(tabAlrm)
	}

	// Список событий (только для клиента)
	if rcnf.isAgentMode == false {
		listBox = gwu.NewListBox([]string{})
		listBox.SetRows(7)
		listBox.Style().SetWidth("350")
		listBox.Style().SetHeight("200")
		listBox.Style().SetBackground("#F0F0E1")
		listBox.SetToolTip("Оповещения о событиях UPS")
		//listBox.Style().SetBorder("#D0E0D0")
		listBox.Style().SetColor(gwu.ClrRed)
		listBox.Style().SetFontSize("16")
		listBox.SetMulti(false)
		listBox.SetEnabled(false)

		win.Add(listBox)
		//countLabel := gwu.NewLabel("Selected count: 0")
		listBox.AddEHandlerFunc(func(e gwu.Event) {
			//countLabel.SetText(fmtabAlrm.Sprintf("Selected count: %d", len(listBox.SelectedIndices())))
			//e.MarkDirty(countLabel)
		}, gwu.ETypeChange)
	}

	// флаги-алармы

	//p.Add(countLabel)

	//win.Add(pan1)

	// алармы

	win.AddVSpace(20)
	win.Add(timelabel)

	timerTime.AddEHandlerFunc(func(e gwu.Event) {
		timelabel.SetText(string("Текущее время: ") + time.Now().Format("15:04:05"))
		e.MarkDirty(timelabel)

		for i, p := range lb {
			if rcnf.isAgentMode == false {
				//fmt.Printf("DISPLAY: %s[%d]\n", val_str[i], i)
				p.SetText(names_str_oid[i] + string(" = ") + val_str[i])
			} else {
				s := fmt.Sprintf(dataFormat[i].c, vf1[i])
				p.SetText(names_str_oid[i] + string(" = ") + s)
			}
			p.Style().SetColor(gwu.ClrGreen)
			//			fmtabAlrm.Printf("%f"+string(" "), vf[i])
			//			fmtabAlrm.Printf("%s"+string(" "), s)
			//			fmtabAlrm.Printf("%s"+string(" \n"), lb[i].Text())

			e.MarkDirty(p)
		}

		if rcnf.isAgentMode == true {
			// Кнопка байпаса - обратная связь
			if buf[sglist[al_BypassActive]] == '1' {
				if bypassState == false {
					bypassState = true
					btnBypass.SetText("Байпас включен")
				}
			} else {
				if bypassState == true {
					bypassState = false
					btnBypass.SetText("Байпас выключен")
				}
			}
			e.MarkDirty(btnBypass)

			// обновляем алармы
			for i, p := range alrmLb {
				if i < const_alarm_MAX {
					if vAlarm[i] != 0 {
						p.Style().SetColor(gwu.ClrRed)
						p.SetText((str_alarmOidsNames[i] + string(" ✓"))) //✓✖
						//p.SetText((oidStrToAlarmTxt[alarmOidStr[i]] + string(" ✓"))) //✓✖
					} else {
						p.SetText(oidStrToAlarmTxt[alarmOidStr[i]])
						//p.SetText((p.Text() + string("")))
						//p.Style().SetColor(gwu.ClrGray)
						p.Style().SetColor("#A0A0A0")
					}
				} else {
					break
				}
				e.MarkDirty(p)
			}
		} else {
			e.MarkDirty(listBox)
		}
	}, gwu.ETypeStateChange)
	win.Add(timerTime)

	//done <- true //подать сигнал готовности

	// Create and start a GUI server (omitting error check)

	//server := gwu.NewServer("UPS", "localhost:8081")

	win.Style().SetBackground("#F0F0E0")
	win.Style().SetBorder("#D0E0D0")
	server.AddWin(win)

	server.Start("main") // Also opens windows list in browser

}
