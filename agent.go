package main

import (
	"fmt"
	"log"
	"net"
	"time"

	"github.com/Logicalis/snmp"
	"github.com/PromonLogicalis/asn1"
	//"github.com/posteo/go-agentx"
)

//static oid upsInputVoltage[] = {1,3,6,1,2,1,33,1,3,3,1,3}; // upsInputVoltage
//static oid upsOutputVoltage[] = {1,3,6,1,2,1,33,1,4,4,1,2}; // upsOutputVoltage
//static oid upsOutputPercentLoad[] = {1,3,6,1,2,1,33,1,4,4,1,5}; // upsOutputPercentLoad
//static oid upsInputFrequency[] = {1,3,6,1,2,1,33,1,3,3,1,2}; // upsInputFrequency
//static oid upsBatteryVoltage[] = {1,3,6,1,2,1,33,1,2,5}; //upsBatteryVoltage
//static oid upsBatteryTemperature[] = {1,3,6,1,2,1,33,1,2,7}; //upsBatteryTemperature
//static oid upsAlarmLowBattery[] = {1,3,6,1,2,1,33,1,6,3,3}; //

func agent_routine() {
	agent := snmp.NewAgent()
	fmt.Print("* go snmp agent *\n")

	// Set the read-only and read-write communities
	agent.SetCommunities("public", "private")
	//agent.SetCommunities("publ", "")

	// Register a read-only OID.
	since := time.Now()
	agent.AddRoManagedObject(
		// sysUpTime
		asn1.Oid{1, 3, 6, 1, 2, 1, 1, 3, 0},
		func(oid asn1.Oid) (interface{}, error) {
			seconds := int(time.Now().Sub(since) / time.Second)
			return seconds, nil
		})

	agent.AddRoManagedObject(
		// sysUpTime
		asn1.Oid{1, 3, 6, 1, 2, 1, 33, 1, 3, 3, 1, 3}, // 1. upsInputVoltage
		func(oid asn1.Oid) (interface{}, error) {
			return int(Vi[0]), nil
		})

	agent.AddRoManagedObject(
		// sysUpTime
		asn1.Oid{1, 3, 6, 1, 2, 1, 33, 1, 4, 4, 1, 2}, // 2. upsOutputVoltage
		func(oid asn1.Oid) (interface{}, error) {
			return int(Vi[1]), nil
		})
	agent.AddRoManagedObject(
		// sysUpTime
		asn1.Oid{1, 3, 6, 1, 2, 1, 33, 1, 4, 4, 1, 5}, // 3. upsOutputPercentLoad
		func(oid asn1.Oid) (interface{}, error) {
			return int(Vi[2]), nil
		})
	agent.AddRoManagedObject(
		// sysUpTime
		asn1.Oid{1, 3, 6, 1, 2, 1, 33, 1, 3, 3, 1, 2}, // 4. upsInputFrequency
		func(oid asn1.Oid) (interface{}, error) {
			return int(Vi[3]), nil
		})
	agent.AddRoManagedObject(
		// sysUpTime
		asn1.Oid{1, 3, 6, 1, 2, 1, 33, 1, 2, 5}, // 5. upsBatteryVoltage
		func(oid asn1.Oid) (interface{}, error) {
			return int(Vi[4]), nil
		})
	agent.AddRoManagedObject(
		// sysUpTime
		asn1.Oid{1, 3, 6, 1, 2, 1, 33, 1, 2, 7}, // 6. upsInputFrequency
		func(oid asn1.Oid) (interface{}, error) {
			return int(Vi[5]), nil
		})
	//	agent.AddRoManagedObject(
	//		// sysUpTime
	//		asn1.Oid{1, 3, 6, 1, 2, 1, 33, 1, 6, 3, 3}, // 7. upsAlarmLowBattery
	//		func(oid asn1.Oid) (interface{}, error) {
	//			return Vi[0], nil
	//		})

	// Register a read-write OID.
	//	name := "example"
	//	agent.AddRwManagedObject(
	//		// sysName
	//		asn1.Oid{1, 3, 6, 1, 2, 1, 1, 5, 0},
	//		func(oid asn1.Oid) (interface{}, error) {
	//			return name, nil
	//		},
	//		func(oid asn1.Oid, value interface{}) error {
	//			strValue, ok := value.(string)
	//			if !ok {
	//				return snmp.VarErrorf(snmp.BadValue, "invalid type")
	//			}
	//			name = strValue
	//			return nil
	//		})

	// Bind to an UDP port
	//addr, err := net.ResolveUDPAddr("udp", ":161")
	addr, err := net.ResolveUDPAddr("udp", ":"+rcnf.snmpAgentPort)
	if err != nil {
		log.Fatal(err)
	}
	conn, err := net.ListenUDP("udp", addr)
	if err != nil {
		log.Fatal(err)
	}

	// Serve requests
	for {
		buffer := make([]byte, 1024)
		n, source, err := conn.ReadFrom(buffer)
		if err != nil {
			log.Fatal(err)
		}

		buffer, err = agent.ProcessDatagram(buffer[:n])
		if err != nil {
			log.Println(err)
			continue
		}

		_, err = conn.WriteTo(buffer, source)
		if err != nil {
			log.Fatal(err)
		}
	}
}

//snmpget -v1 -c public localhost .1.3.6.1.2.1.33.1.3.3.1.3
