// read_data
// читаем из посл.порта, пишем в массивы аналоговых значений и алармов, шлем трапы.
package main

import (
	//"bytes"
	"fmt"
	"log"
	"math"
	"strconv"
	"time"

	"github.com/posteo/go-agentx"
	"github.com/posteo/go-agentx/pdu"
	"github.com/robfig/cron"
	"github.com/tarm/serial"
)

// число oid SNMP
const const_SNMP_OID_MAX = 7

// количество статусных байтов в входном буфере, приходящих из UPS,
// (+ еще один sg_CommunicationsLost, который формируем сами )
const const_status_MAX = 7

// кол-во типов oid'ов для таблицы алармов
const const_alarm_MAX = const_status_MAX // !! Равно

// аналоговые значения из ups в формате с плав.точкой
var vf [agentx.Const_data_MAX]float64
var vf1 [lb_val_N]float64

//текущие алармы
var vAlarm = [const_alarm_MAX]int32{0, 0, 0, 0, 0, 0, 0}
var vAlarm_old = [const_alarm_MAX]int32{0, 0, 0, 0, 0, 0, 0}
var timeOnBatteryLast time.Time

const (
	//	sg_UtilityFail    = 37
	//	sg_BatteryLow     = 38
	//	sg_BypassActive   = 39
	//	sg_UpsFailed      = 40
	//	sg_TestInProgress = 42
	//	sg_ShutDownActive = 43

	sg_UtilityFail    = 38
	sg_BatteryLow     = 39
	sg_BypassActive   = 40
	sg_UpsFailed      = 41
	sg_TestInProgress = 43
	sg_ShutDownActive = 44
	// такого нет из UPS, конечно (44 байт, на этом месте <cr>),
	//но байт-статус  будем выставлять сами тоже тут:
	sg_CommunicationsLost = 45
)

var sglist = [const_alarm_MAX]int{
	sg_UtilityFail, sg_BatteryLow, sg_BypassActive,
	sg_UpsFailed, sg_TestInProgress, sg_ShutDownActive, sg_CommunicationsLost}

const (
	al_UtilityFail = iota // Авария сети s_upsAlarmInputBad
	al_BatteryLow
	al_BypassActive
	al_UpsFailed
	al_TestInProgress
	al_ShutDownActive
	al_CommunicationLost
)

var str_alarmOidsNames = []string{
	"upsAlarmInputBad",
	"upsAlarmLowBattery",
	"upsAlarmOnBypass",
	"upsAlarmGeneralFault",
	"upsAlarmTestInProgress",
	"upsAlarmShutdownImminent",
	"upsAlarmCommunicationsLost",
}

// соотв.входных статустных байтов из UPS и oid'ов в таблице алармов
var s_alarmOids = []string{
	s_upsAlarmInputBad, //s_upsAlarmOnBattery тоже
	s_upsAlarmLowBattery,
	s_upsAlarmOnBypass,
	s_upsAlarmGeneralFault,
	s_upsAlarmTestInProgress,
	s_upsAlarmShutdownImminent,
	s_upsAlarmCommunicationsLost,
}

var oidStrToAlarmTxt = map[string]string{
	s_upsAlarmOnBattery:          "Питание от батареи",
	s_upsAlarmLowBattery:         "Батарея разряжена",
	s_upsAlarmInputBad:           "Авария сети",
	s_upsAlarmOnBypass:           "Питание через байпас",
	s_upsAlarmGeneralFault:       "Общая авария",
	s_upsAlarmCommunicationsLost: "Нет связи с ИБП",
	s_upsAlarmTestInProgress:     "Идет тест",
	s_upsAlarmShutdownImminent:   "Выключение компьютера!",
}

// Проекция вх.массива байтов из посл.порта, на соотв. им #oid'ов,
// которые будут отсылаться в служ. поле трапа upsTrapAlarmEntryAdded/Removed.
// Примеч.: s_upsAlarmInputBad попадает в таблицу, проихсодит по тому же вх.байту
// (№37 = sg_UtilityFail посл.порта), что и upsAlarmOnBattery, но upsAlarmOnBattery
// в таблицу трапов не попадает
var sgToOidStr = [const_status_MAX][]string{
	{s_upsAlarmInputBad /*, s_upsAlarmOnBattery*/},
	{s_upsAlarmLowBattery},
	{s_upsAlarmOnBypass},
	{s_upsAlarmGeneralFault},
	{s_upsAlarmTestInProgress},
	{s_upsAlarmShutdownImminent},
	{s_upsAlarmCommunicationsLost},
}

type thepair struct {
	a, b int    // № Начального и конечного  байтов
	c    string // строка формата для printf
	d    int    // кол-во знаков до запятой
	e    int    // кол-во знаков после запятой
}

// описание формата аналоговых данных из ups
var dataFormat = [agentx.Const_data_MAX]thepair{
	{1, 6, "%3.1f", 3, 1},   //MMM.M
	{7, 12, "%3.1f", 3, 1},  //NNN.N
	{13, 18, "%3.1f", 3, 1}, //PPP.P
	{19, 22, "%3.0f", 3, 0}, //QQQ
	{23, 27, "%2.1f", 2, 1}, //RR.R
	{28, 32, "%1.2f", 1, 2}, //S.SS
	{33, 37, "%2.1f", 2, 1}, //TT.T
}

// 0=передача
const (
	upsTxGetData   = 0
	upsTxBypassOff = 1
	upsTxBypassOn  = 2
)

var bypassState bool = false

// общее количество алармов
var alarmCnt uint32 = 0
var alarmN int = 0
var buf []byte

var serialport *serial.Port

func serial_routine(port string) {
	timeOnBatteryLast = time.Now()
	//var data_OnOffBypass = map[int]string{upsTxGetData: "Q1\n", upsTxBypassOff: "BO0\n\r", upsTxBypassOn: "BO1\n\r"}
	buf = make([]byte, 128)

	c := &serial.Config{Name: port, Baud: 2400, ReadTimeout: time.Second}
	serialp, err := serial.OpenPort(c)
	serialport = serialp
	if err != nil {
		log.Fatal(err)
	}

	fmt.Printf("tty is open\n")

	//	timerBypassOn := time.NewTimer(time.Second * 5)
	//	timerBypassOff := time.NewTimer(time.Second * 10)
	//	go func() {
	//		<-timerBypassOn.C
	//		fmt.Println("Timer 5sec expired")
	//		_, err := serialport.Write([]byte("BO1\n\r"))
	//		if err != nil {
	//			log.Fatal(err)
	//		}
	//		<-timerBypassOff.C
	//		fmt.Println("Timer 10sec expired")
	//		_, err1 := serialport.Write([]byte("BO0\n\r"))
	//		if err != nil {
	//			log.Fatal(err1)
	//		}

	//	}()

	// СRON'ed func (every 1m):
	cr := cron.New()
	cr.AddFunc("@every 61s", func() {
		for _, p := range rcnf.targetAddr {
			if p == "" {
				continue
			}
			trapSend2_upsTrapOnBattery(p, (time.Now().Sub(timeOnBatteryLast)).Seconds()) // trap: новый аларм добавлен в таблицу алармов
		}
		//fmt.Println("croned @ 61s!")
	})

	for ivn := 0; ; ivn++ {
		sendStr := "Q1" + string(13)
		//_, err := serialport.Write([]byte("Q1"))
		_, err := serialport.Write([]byte(sendStr))
		if err != nil {
			log.Fatal(err)
		}

		time.Sleep(time.Millisecond * 1000)
		//fmt.Printf("..next interval(%d)\n", ivn)
		_ /*n_read*/, err_read := serialport.Read(buf)
		if err_read != nil {
			//log.Fatal(err_read)
			//log.Println(err_read)
		}

		// ...если buf[0] != '(', то upsAlarmCommunicationsLost
		// и считано 45(?) байтов, то ок
		if buf[0] != '(' /*|| n_read != 44*/ {
			buf[sg_CommunicationsLost] = '1'
		} else {
			buf[sg_CommunicationsLost] = '0'
		}
		buf[0] = '_'

		//log.Printf("data[%d]= %s\n", n, buf[:n])

		//(MMM.M * NNN.N * PPP.P * QQQ * RR.R * S.SS * TT.T * U<cr>

		// добываем численные значения
		for i, p := range dataFormat {
			str := string(buf[p.a:p.b])
			//fmt.Printf("%s\n", str)
			v, err := strconv.ParseFloat(str, 32)
			vf[i] = v
			if err == nil {
				//				fmt.Printf(p.c, v)
				//				fmt.Printf("\n")
			}
			//Vi[i] = int32(vf[ii[i]]*math.Pow10(p.e) + 0.5)
			Vi[i] = int32(vf[i] * math.Pow10(p.e))
			//fmt.Printf("%d=%d\n", i, Vi[i]) // печать значений аналоговых oid'ов
		}

		// переписываем в vf1 , для -a -gui (в реж.агента)
		for i, _ := range vf {
			if i < 1 {
				vf1[i] = vf[i]
			} else if i < lb_val_N {
				vf1[i] = vf[i+1]
			} else {
				break
			}
		}
		vf1[4] = float64(rcnf.batteryVoltage) * vf[5] / 2

		// пробегаем по статусным байтам входного буфера
		for i := 0; i < const_status_MAX; i++ {
			//fmt.Printf(" %d = %q,  ", i, buf[sglist[i]])
			if buf[sglist[i]] == '1' {
				if vAlarm[i] == 0 { // это новый alarm
					vAlarm[i] = 1
					//fmt.Printf(" %d = %d,  ", i, vAlarm[i])

					alarmCnt++ // общее количество алармов
					alarmN++   // Порядковый номер аларма в списке (1..6)
					//fmt.Printf("ALARMCNT=%d", alarmCnt)
					strCur := fmt.Sprintf(".%d", alarmN)

					new_upsAlarmId := s_upsAlarmEntry + string(".1") + strCur
					item := listHandler.Add(new_upsAlarmId)
					item.Type = pdu.VariableTypeInteger
					item.Value = int32(alarmCnt)
					//fmt.Printf("new alarmId[%d]: %s==%s\n", alarmN, new_upsAlarmId, item.Value)

					new_upsAlarmDescr := s_upsAlarmEntry + string(".2") + strCur
					item = listHandler.Add(new_upsAlarmDescr)
					item.Type = pdu.VariableTypeObjectIdentifier
					item.Value = s_alarmOids[i]
					//fmt.Printf("new alarmDescr[%d]: %s==%s\n", alarmN, new_upsAlarmDescr, oidStrToAlarmTxt[s_alarmOids[i]])

					new_upsAlarmTime := s_upsAlarmEntry + string(".3") + strCur
					item = listHandler.Add(new_upsAlarmTime)
					item.Type = pdu.VariableTypeTimeTicks
					item.Value = time.Now().Sub(agentxStartTime)
					//fmt.Printf("new alarmTime[%d]: %s==%d\n", alarmN, new_upsAlarmTime, item.Value)
					//fmt.Printf("after add: %+v\n", listHandler.items)

					if i == al_UtilityFail { // Авария сети, переход на батарею
						batteryStartTime = time.Now()
						// пишем в таблицу алармов, по тому же статусному байту, что и s_upsAlarmInputBad
						alarmCnt++ // общее количество алармов
						alarmN++   // Порядковый номер аларма в списке (1..7)
						//fmt.Printf("ALARMCNT=%d", alarmCnt)
						strCur := fmt.Sprintf(".%d", alarmN)

						new_upsAlarmId := s_upsAlarmEntry + string(".1") + strCur
						item := listHandler.Add(new_upsAlarmId)
						item.Type = pdu.VariableTypeInteger
						item.Value = int32(alarmCnt)
						//fmt.Printf("new alarmId[%d]: %s==%s\n", alarmN, new_upsAlarmId, item.Value)

						new_upsAlarmDescr := s_upsAlarmEntry + string(".2") + strCur
						item = listHandler.Add(new_upsAlarmDescr)
						item.Type = pdu.VariableTypeObjectIdentifier
						item.Value = s_upsAlarmOnBattery
						//fmt.Printf("new alarmDescr[%d]: %s==%s\n", alarmN, new_upsAlarmDescr, oidStrToAlarmTxt[s_alarmOids[i]])

						new_upsAlarmTime := s_upsAlarmEntry + string(".3") + strCur
						item = listHandler.Add(new_upsAlarmTime)
						item.Type = pdu.VariableTypeTimeTicks
						item.Value = time.Now().Sub(agentxStartTime)
						//fmt.Printf("new alarmTime[%d]: %s==%d\n", alarmN, new_upsAlarmTime, item.Value)
						//fmt.Printf("after add: %+v\n", listHandler.items)

						// рассылаем трап upsTrapOnBattery по всем ip-адресам
						for _, p := range rcnf.targetAddr {
							if p == "" {
								continue
							}
							trapSend2_upsTrapOnBattery(p, 0) // trap: новый аларм добавлен в таблицу алармов
						}
						timeOnBatteryLast = time.Now()
						// запустить cron, чтобы периодич.повторять (T=60с)
						cr.Start()

						//fmt.Println("cron run!")
					} else if i != al_TestInProgress {
						// рассылаем остальные трапы  (кроме upsTrapOnBattery) по всем ip-адресам
						for _, p := range rcnf.targetAddr {
							if p == "" {
								continue
							}
							trapSend2(p, s_upsTrapAlarmEntryAdded,
								s_upsAlarmEntry+string(".1")+strCur, alarmCnt,
								s_upsAlarmEntry+string(".2")+strCur, s_alarmOids[i])
							//							fmt.Printf("trapSend2(%s,%s,%s,%s,%s,%s\n", p, s_upsTrapAlarmEntryAdded,
							//								s_upsAlarmEntry+string(".1")+strCur, alarmCnt,
							//								s_upsAlarmEntry+string(".2")+strCur, s_alarmOids[i])

						}
					}

					// upsTrapOnBattery  1.3.6.1.2.1.33.2.1
					//The UPS is operating on battery power. This trap is
					//persistent and is resent at one minute intervals until
					//the UPS either turns off or is no longer running on
					//battery.

				}
			} else { // buf[sglist[i]] == '0'
				if vAlarm[i] == 1 { // удалить alarm #<i>, i={0...5}
					// Убираем флаг наличия для соотв. аларма
					vAlarm[i] = 0
					// 1. очищаем items, создавая новый
					listHandler.items = nil
					// 2. Заново добавляем все oid'ы (вот так, да)
					add_oids()
					// 3. снова пробегаем все байт-флаги и добавляем оставшиеся алармы обратно: формируем заново oid для таблицы alarm'ов
					//fmt.Printf("renew alarms:\n")
					alarmN = 0
					for j := 0; j < const_status_MAX; j++ {
						if vAlarm[j] == 1 { // для активных алармов
							strCur := fmt.Sprintf(".%d", alarmN+1)
							new_upsAlarmId := s_upsAlarmEntry + string(".1") + strCur
							item := listHandler.Add(new_upsAlarmId)
							item.Type = pdu.VariableTypeInteger
							item.Value = s_alarmOids[i]
							//fmt.Printf("renew alarmId[%d]: %s==%s\n", alarmN, new_upsAlarmId, item.Value)

							new_upsAlarmDescr := s_upsAlarmEntry + string(".2") + strCur
							item = listHandler.Add(new_upsAlarmDescr)
							//item.Type = pdu.VariableTypeOctetString
							item.Type = pdu.VariableTypeObjectIdentifier
							item.Value = s_alarmOids[i]
							//fmt.Printf("renew alarmDescr[%d]: %s\n", alarmN, oidStrToAlarmTxt[s_alarmOids[alarmN]])

							new_upsAlarmTime := s_upsAlarmEntry + string(".3") + strCur
							item = listHandler.Add(new_upsAlarmTime)
							item.Type = pdu.VariableTypeTimeTicks
							item.Value = time.Now().Sub(agentxStartTime)
							//fmt.Printf("renew alarmTime[%d]: %s==%s\n", alarmN, new_upsAlarmTime, item.Value)
							alarmN++
							//fmt.Printf("after add: %+v\n", listHandler.items)
						}
					}
					//fmt.Printf("after delete, alarmN=%d\n", alarmN)
					if i != al_UtilityFail && i != al_TestInProgress {
						//strCur := fmt.Sprintf(".%d", alarmN)
						strCur := fmt.Sprintf(".%d", alarmN)
						for _, p := range rcnf.targetAddr {
							if p == "" {
								continue
							}
							trapSend2(p, s_upsTrapAlarmEntryRemoved,
								s_upsAlarmEntry+string(".1")+strCur, alarmCnt,
								s_upsAlarmEntry+string(".2")+strCur, s_alarmOids[i])
							//							fmt.Printf("trapSend2(%s,%s,%s,%s,%s,%s\n", p, s_upsTrapAlarmEntryRemoved,
							//								s_upsAlarmEntry+string(".1")+strCur, alarmCnt,
							//								s_upsAlarmEntry+string(".2")+strCur, s_alarmOids[i])

						}
					}
					// если UtilityFail снова в норме (=0), то остановить ежеминутную отправку трапов upsOnBattery
					if i == al_UtilityFail {
						cr.Stop()
					}
					alarmCnt-- // общее количество алармов
				}
			}
		}
		//fmt.Println("")

	}
}
