package main

import (
	"log"
	//	"net"
	//	"fmt"
	//	"math"
	//	"strconv"
	//"fmt"
	"time"

	"github.com/posteo/go-agentx"
	"github.com/posteo/go-agentx/pdu"
	"github.com/posteo/go-agentx/value"
	"gopkg.in/errgo.v1"
)

const (
	// все string
	s_upsObjects                   = string("1.3.6.1.2.1.33.1")
	s_upsIdent                     = string("1.3.6.1.2.1.33.1.1")
	s_upsIdentManufacturer         = string("1.3.6.1.2.1.33.1.1.1.0")
	s_upsIdentModel                = string("1.3.6.1.2.1.33.1.1.2.0")
	s_upsIdentUPSSoftwareVersion   = string("1.3.6.1.2.1.33.1.1.3.0")
	s_upsIdentAgentSoftwareVersion = string("1.3.6.1.2.1.33.1.1.4.0")
	s_upsIdentName                 = string("1.3.6.1.2.1.33.1.1.5.0")
	s_upsIdentAttachedDevices      = string("1.3.6.1.2.1.33.1.1.6.0")

	// все int32
	s_upsBattery                   = string("1.3.6.1.2.1.33.1.2")
	s_upsBatteryStatus             = string("1.3.6.1.2.1.33.1.2.1.0") //(1-unknown, 2-batteryNormal, 3-batteryLow, 4-batteryDepleted)
	s_upsSecondsOnBattery          = string("1.3.6.1.2.1.33.1.2.2.0")
	s_upsEstimatedMinutesRemaining = string("1.3.6.1.2.1.33.1.2.3.0")
	s_upsEstimatedChargeRemaining  = string("1.3.6.1.2.1.33.1.2.4.0")
	s_upsBatteryVoltage            = string("1.3.6.1.2.1.33.1.2.5.0")
	s_upsBatteryCurrent            = string("1.3.6.1.2.1.33.1.2.6.0")
	s_upsBatteryTemperature        = string("1.3.6.1.2.1.33.1.2.7.0")

	s_upsInput         = string("1.3.6.1.2.1.33.1.3")
	s_upsInputLineBads = string("1.3.6.1.2.1.33.1.3.1.0")
	s_upsInputNumLines = string("1.3.6.1.2.1.33.1.3.2.0")
	//s_upsInputTable     = string("1.3.6.1.2.1.33.1.3.3")
	//s_upsInputEntry     = string("1.3.6.1.2.1.33.1.3.3.1")
	s_upsInputLineIndex = string("1.3.6.1.2.1.33.1.3.3.1.1.1")
	s_upsInputFrequency = string("1.3.6.1.2.1.33.1.3.3.1.2.1")
	s_upsInputVoltage   = string("1.3.6.1.2.1.33.1.3.3.1.3.1")
	s_upsInputCurrent   = string("1.3.6.1.2.1.33.1.3.3.1.4.1")
	s_upsInputTruePower = string("1.3.6.1.2.1.33.1.3.3.1.5.1")

	s_upsOutput          = string("1.3.6.1.2.1.33.1.4")
	s_upsOutputSource    = string("1.3.6.1.2.1.33.1.4.1.0")
	s_upsOutputFrequency = string("1.3.6.1.2.1.33.1.4.2.0")
	s_upsOutputNumLines  = string("1.3.6.1.2.1.33.1.4.3.0")
	//	s_upsOutputTable       = string("1.3.6.1.2.1.33.1.4.4")
	//	s_upsOutputEntry       = string("1.3.6.1.2.1.33.1.4.4.1")
	s_upsOutputLineIndex   = string("1.3.6.1.2.1.33.1.4.4.1.1.1")
	s_upsOutputVoltage     = string("1.3.6.1.2.1.33.1.4.4.1.2.1")
	s_upsOutputCurrent     = string("1.3.6.1.2.1.33.1.4.4.1.3.1")
	s_upsOutputPower       = string("1.3.6.1.2.1.33.1.4.4.1.4.1")
	s_upsOutputPercentLoad = string("1.3.6.1.2.1.33.1.4.4.1.5.1")

	s_upsAlarmOnBattery          = string("1.3.6.1.2.1.33.1.6.3.2")  //Питание от батареи
	s_upsAlarmLowBattery         = string("1.3.6.1.2.1.33.1.6.3.3")  //Батарея разряжена
	s_upsAlarmInputBad           = string("1.3.6.1.2.1.33.1.6.3.6")  //Авария сети
	s_upsAlarmOnBypass           = string("1.3.6.1.2.1.33.1.6.3.9")  //Питание через байпас
	s_upsAlarmGeneralFault       = string("1.3.6.1.2.1.33.1.6.3.18") //Общая авария
	s_upsAlarmCommunicationsLost = string("1.3.6.1.2.1.33.1.6.3.20") //Нет связи с ИБП
	s_upsAlarmShutdownPending    = string("1.3.6.1.2.1.33.1.6.3.22") //A upsShutdownAfterDelay countdown is underway.
	s_upsAlarmShutdownImminent   = string("1.3.6.1.2.1.33.1.6.3.23") //The UPS will turn off power to the load in less than 5 seconds; this may be either a timed shutdown or a low battery shutdown.
	s_upsAlarmTestInProgress     = string("1.3.6.1.2.1.33.1.6.3.24") //A test is in progress

	s_upsAlarmsPresent = string("1.3.6.1.2.1.33.1.6.1")
	s_upsAlarmTable    = string("1.3.6.1.2.1.33.1.6.2")
	s_upsAlarmEntry    = string("1.3.6.1.2.1.33.1.6.2.1")

	s_upsAlarmId1    = string("1.3.6.1.2.1.33.1.6.2.1.1.1")
	s_upsAlarmDescr1 = string("1.3.6.1.2.1.33.1.6.2.1.2.1")
	s_upsAlarmTime1  = string("1.3.6.1.2.1.33.1.6.2.1.3.1")

	s_upsAlarmId2    = string("1.3.6.1.2.1.33.1.6.2.1.1.2")
	s_upsAlarmDescr2 = string("1.3.6.1.2.1.33.1.6.2.1.2.2")
	s_upsAlarmTime2  = string("1.3.6.1.2.1.33.1.6.2.1.3.2")

	s_upsAlarmId3    = string("1.3.6.1.2.1.33.1.6.2.1.1.3")
	s_upsAlarmDescr3 = string("1.3.6.1.2.1.33.1.6.2.1.2.3")
	s_upsAlarmTime3  = string("1.3.6.1.2.1.33.1.6.2.1.3.3")

	s_upsAlarmId4    = string("1.3.6.1.2.1.33.1.6.2.1.1.4")
	s_upsAlarmDescr4 = string("1.3.6.1.2.1.33.1.6.2.1.2.4")
	s_upsAlarmTime4  = string("1.3.6.1.2.1.33.1.6.2.1.3.4")

	s_upsAlarmId5    = string("1.3.6.1.2.1.33.1.6.2.1.1.5")
	s_upsAlarmDescr5 = string("1.3.6.1.2.1.33.1.6.2.1.2.5")
	s_upsAlarmTime5  = string("1.3.6.1.2.1.33.1.6.2.1.3.5")

	s_upsAlarmId6    = string("1.3.6.1.2.1.33.1.6.2.1.1.6")
	s_upsAlarmDescr6 = string("1.3.6.1.2.1.33.1.6.2.1.2.6")
	s_upsAlarmTime6  = string("1.3.6.1.2.1.33.1.6.2.1.3.6")

	s_upsTrapOnBattery         = string(".1.3.6.1.2.1.33.2.1")
	s_upsTrapTestCompleted     = string(".1.3.6.1.2.1.33.2.2")
	s_upsTrapAlarmEntryAdded   = string(".1.3.6.1.2.1.33.2.3")
	s_upsTrapAlarmEntryRemoved = string(".1.3.6.1.2.1.33.2.4")
)

var ones int = 42

var client1 agentx.Client

var listHandler *ListHandler
var session *agentx.Session

var agentxStartTime, batteryStartTime time.Time

var s_map = map[string]string{
	//	s_upsObjects:                   "",
	//	s_upsIdent:                     "",
	s_upsIdentManufacturer:         "OOO Promsviazdizain",
	s_upsIdentModel:                "Titan",
	s_upsIdentUPSSoftwareVersion:   "0",
	s_upsIdentAgentSoftwareVersion: "v1.0",
	s_upsIdentName:                 "<...>",
	s_upsIdentAttachedDevices:      "<...>",
}
var int_map = map[string]int{
	s_upsInputLineBads: 0, //1, 0
	s_upsInputNumLines: 1, //
	//s_upsInputTable
	//s_upsInputEntry
	s_upsInputLineIndex: 1,
	s_upsInputFrequency: 0,
	s_upsInputVoltage:   0,
	s_upsInputCurrent:   0,
	s_upsInputTruePower: 0,

	//s_upsOutput: ,
	s_upsOutputSource:    1, //other(1), none(2), normal(3), bypass(4), battery(5)
	s_upsOutputFrequency: 0,
	s_upsOutputNumLines:  1,
	//s_upsOutputTable: ,
	//s_upsOutputEntry: ,
	s_upsOutputLineIndex:   1,
	s_upsOutputVoltage:     0,
	s_upsOutputCurrent:     0,
	s_upsOutputPower:       0,
	s_upsOutputPercentLoad: 0, //=% загрузки от полной мощности
}

func add_oids() {
	// 1. очищаем items, создавая новый
	// listHandler.items = nil
	// 2. Заново добавляем все oid'ы (вот так, да)
	type oidWithType struct {
		s_oid string
		t_oid int
		//val   string
	}
	const (
		ot_int = iota
		ot_uint
		ot_gauge
		ot_str
		ot_oid
	)
	var oids_All = []struct {
		s_oid string
		t_oid int
	}{
		//{s_upsIdent, ot_str},
		{s_upsIdentManufacturer, ot_str},
		{s_upsIdentModel, ot_str},
		{s_upsIdentUPSSoftwareVersion, ot_str},
		{s_upsIdentAgentSoftwareVersion, ot_str},
		{s_upsIdentName, ot_str},
		{s_upsIdentAttachedDevices, ot_str},

		//s_upsBattery,
		{s_upsBatteryStatus, ot_int},
		{s_upsSecondsOnBattery, ot_int},
		{s_upsEstimatedMinutesRemaining, ot_int},
		{s_upsEstimatedChargeRemaining, ot_int},
		{s_upsBatteryVoltage, ot_int},
		{s_upsBatteryCurrent, ot_int},
		{s_upsBatteryTemperature, ot_int},

		//{s_upsInput, ot_int},
		{s_upsInputLineBads, ot_uint},
		{s_upsInputNumLines, ot_int},
		//		{s_upsInputTable, ot_int},
		//		{s_upsInputEntry, ot_int},
		{s_upsInputLineIndex, ot_int},
		{s_upsInputFrequency, ot_int},
		{s_upsInputVoltage, ot_int},
		{s_upsInputCurrent, ot_int},
		{s_upsInputTruePower, ot_int},

		//{s_upsOutput, ot_uint},
		{s_upsOutputSource, ot_int},
		{s_upsOutputFrequency, ot_int},
		{s_upsOutputNumLines, ot_int},
		//		{s_upsOutputTable, ot_uint},
		//		{s_upsOutputEntry, ot_uint},
		{s_upsOutputLineIndex, ot_int},
		{s_upsOutputVoltage, ot_int},
		{s_upsOutputCurrent, ot_int},
		{s_upsOutputPower, ot_int},
		{s_upsOutputPercentLoad, ot_int},

		{s_upsAlarmsPresent, ot_gauge},
	}

	for _, p := range oids_All {
		//fmt.Printf("%s=%d\n", p.s_oid, p.t_oid)
		item := listHandler.Add(p.s_oid)
		switch p.t_oid {
		case ot_uint:
			item.Type = pdu.VariableTypeCounter32
			item.Value = uint32(int_map[p.s_oid])
		case ot_int:
			item.Type = pdu.VariableTypeInteger
			item.Value = int32(int_map[p.s_oid])
		case ot_gauge:
			item.Type = pdu.VariableTypeGauge32
			item.Value = uint32(int_map[p.s_oid])
		case ot_str:
			item.Type = pdu.VariableTypeOctetString
			item.Value = s_map[p.s_oid]
		case ot_oid:
			item.Type = pdu.VariableTypeObjectIdentifier
			item.Value = make([]int32, 10)
		}

	}
}

func init_agent_x() {
	agentxStartTime = time.Now()
	client1 = agentx.Client{
		Net:               "tcp",
		Address:           "localhost:705",
		Timeout:           1 * time.Minute,
		ReconnectInterval: 1 * time.Second,
	}

	if err := client1.Open(); err != nil {
		log.Fatalf(errgo.Details(err))
	}

	session, err := client1.Session()
	if err != nil {
		log.Fatalf(errgo.Details(err))
	}

	listHandler = &ListHandler{}

	add_oids()
	session.Handler = listHandler

	if err := session.Register(127, value.MustParseOID("1.3.6.1.2.1.33.1")); err != nil {
		log.Fatalf(errgo.Details(err))
	}
}
