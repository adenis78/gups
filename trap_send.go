package main

import (
	"log"
	"net"
	"os"
	"strconv"
	"time"
	//	"github.com/davecgh/go-spew/spew"

	g "github.com/soniah/gosnmp"
)

func getLocalIP() string {
	addrs, err := net.InterfaceAddrs()
	if err != nil {
		return ""
	}
	for _, address := range addrs {
		// check the address type and if it is not a loopback the display it
		if ipnet, ok := address.(*net.IPNet); ok && !ipnet.IP.IsLoopback() {
			if ipnet.IP.To4() != nil {
				return ipnet.IP.String()
			}
		}
	}
	return ""
}

func trapSend() {

	// Default is a pointer to a GoSNMP struct that contains sensible defaults
	// eg port 161, community public, etc
	g.Default.Target = "127.0.0.1"
	g.Default.Port = 162
	g.Default.Version = g.Version2c
	g.Default.Community = rcnf.community //"public"
	g.Default.Logger = log.New(os.Stdout, "", 0)

	err := g.Default.Connect()
	if err != nil {
		log.Fatalf("Connect() err: %v", err)
	}
	defer g.Default.Conn.Close()

	pdu := g.SnmpPDU{
		Name:  ".1.3.6.1.6.3.1.1.4.1.0",
		Type:  g.ObjectIdentifier,
		Value: ".1.3.6.1.6.3.1.1.5.1",
	}

	trap := g.SnmpTrap{
		Variables: []g.SnmpPDU{pdu},
	}

	_, err = g.Default.SendTrap(trap)
	if err != nil {
		log.Fatalf("SendTrap() err: %v", err)
	}
}

func trapSend2(
	str_defaultTarget, // ip address приемника
	name_str, /*upsTrapAlarmOnBattery ИЛИ upsTrapAlarmEntryAdded ИЛИ upsTrapAlarmEntryRemoved*/
	str_alarmId string,
	int_val_alarmOid uint32,
	str_alarmDescr,
	str_val_alarmDescr string) {

	// Default is a pointer to a GoSNMP struct that contains sensible defaults
	// eg port 161, community public, etc
	//g.Default.Target = "127.0.0.1"
	//g.Default.Target = "192.168.0.20"
	g.Default.Target = str_defaultTarget
	var err error
	port32, err := strconv.ParseUint(rcnf.snmpTrapPort, 10, 16)
	g.Default.Port = uint16(port32)
	g.Default.Version = g.Version2c
	g.Default.Community = rcnf.community //"public"
	//g.Default.Logger = log.New(os.Stdout, "", 0)

	err = g.Default.Connect()
	if err != nil {
		log.Fatalf("Connect() err: %v", err)
	}
	defer g.Default.Conn.Close()

	pdu0 := g.SnmpPDU{
		Name: ".1.3.6.1.6.3.1.1.4.1.0",
		//Type: g.OctetString,
		Type: g.ObjectIdentifier,
		//Type:  type_str,
		Value: name_str, //upsTrapAlarmEntryAdded|upsTrapAlarmEntryRemoved
	}
	pdu1 := g.SnmpPDU{
		Name: str_alarmId,
		Type: g.Integer,
		//Type:  type_str,
		Value: int(int_val_alarmOid),
	}
	pdu2 := g.SnmpPDU{
		Name: str_alarmDescr,
		Type: g.ObjectIdentifier,
		//Type:  type_str,
		Value: str_val_alarmDescr,
	}
	pdu_00 := g.SnmpPDU{
		Name: ".1.3.6.1.2.1.1.3.0",
		Type: g.TimeTicks,
		//Type:  type_str,
		Value: uint32(time.Now().Sub(startTime).Nanoseconds() / 10000000),
	}

	//fmt.Printf("%+v\n%+v\n%+v\n", pdu0, pdu1, pdu2)
	//	spew.Dump(pdu0)
	//	spew.Dump(pdu1)
	//	spew.Dump(pdu2)
	trap := g.SnmpTrap{
		Variables: []g.SnmpPDU{pdu_00, pdu0, pdu1, pdu2},
	}

	_, err = g.Default.SendTrap(trap)
	if err != nil {
		log.Fatalf("SendTrap() err: %v", err)
	}
}

func trapSend2_upsTrapOnBattery(
	str_defaultTarget string, // ip address приемника
	secondsOnBattery float64,
) {

	// Default is a pointer to a GoSNMP struct that contains sensible defaults
	// eg port 161, community public, etc
	//g.Default.Target = "127.0.0.1"
	//g.Default.Target = "192.168.0.20"
	g.Default.Target = str_defaultTarget
	var err error
	port32, err := strconv.ParseUint(rcnf.snmpTrapPort, 10, 16)
	g.Default.Port = uint16(port32)
	g.Default.Version = g.Version2c
	g.Default.Community = rcnf.community //"public"
	//g.Default.Logger = log.New(os.Stdout, "", 0)

	err = g.Default.Connect()
	if err != nil {
		log.Fatalf("Connect() err: %v", err)
	}
	defer g.Default.Conn.Close()

	pdu0 := g.SnmpPDU{
		Name: ".1.3.6.1.6.3.1.1.4.1.0",
		Type: g.ObjectIdentifier,
		//Type:  type_str,
		Value: s_upsTrapOnBattery,
	}
	pdu1 := g.SnmpPDU{
		Name: s_upsEstimatedMinutesRemaining,
		Type: g.Integer,
		//Type:  type_str,
		Value: int(0),
	}
	pdu2 := g.SnmpPDU{
		Name: s_upsSecondsOnBattery,
		Type: g.Integer,
		//Type:  type_str,
		Value: int(secondsOnBattery),
	}
	pdu3 := g.SnmpPDU{
		Name: "1.3.6.1.2.1.33.1.9.7.0", //s_upsConfigLowBattTime,
		Type: g.Integer,
		//Type:  type_str,
		Value: int(0),
	}
	pdu_00 := g.SnmpPDU{
		Name: ".1.3.6.1.2.1.1.3.0",
		Type: g.TimeTicks,
		//Type:  type_str,
		Value: uint32(time.Now().Sub(startTime).Nanoseconds() / 10000000),
	}

	trap := g.SnmpTrap{
		Variables: []g.SnmpPDU{pdu_00, pdu0, pdu1, pdu2, pdu3},
	}

	_, err = g.Default.SendTrap(trap)
	if err != nil {
		log.Fatalf("SendTrap() err: %v", err)
	}
}
